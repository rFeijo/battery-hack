// Bibliotecas
  #include "graphic_interface.h"

// Instância da Classe OLED
  Interface interface;

void setup(void)
{
  // Inicializa a Interface Gráfica
  interface.begin();

  // Espera até que uma bateria seja conectada
  interface.charge_battery();

  // Configura a corrente de teste
  interface.set_discharge_current();

  // Inicializa teste
  interface.discharge_battery();
}

void loop(void) {}
