#include "graphic_interface.h"

// Instância da Classe Adafruit_SSD1306
  Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// Instâncias da Classe Button
  Button up_button(2, 25, false, true);
  Button down_button(3, 25, false, true);

// Classe Interface::inicializa a interface gráfica e apresenta a tela inicial
  void Interface::begin(void)
  {
    // Inicializa o pino de saída de tensão
    pinMode(voltage_control_pin, OUTPUT);
    analogWrite(voltage_control_pin, 0x00);

    // Inicializa o Buzzer
    pinMode(buzzer_control_pin, OUTPUT);

    // Configura o display
    display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
    display.clearDisplay();
    display.setTextColor(WHITE); 

    // Apresenta a tela inicial
    display.setTextSize(2);
    display.setCursor(20, 10);
    display.print("Battery");
    display.setCursor(40, 30);
    display.print("Hack");
    display.setCursor(40, 50);
    display.print("v1.0");
    display.display();

    // Apresenta sinal no Buzzer
    play_buzzer(1, 1000, 0);

    // Inicializa os botões de controle do sistema
    up_button.begin();
    down_button.begin();

    VCC = measure_voltage(vcc_pin, 30);
  }

// Classe Interface::espera até que a bateria esteja carregada (> +4,15V)
  void Interface::charge_battery(void)
  {
    // Reseta as veriáveis do RTC
    reset_timer();

    // Realiza o teste de descarga
    while (!charged)
    {
      // Le a tensão da bateria
      battery_voltage = measure_voltage(battery_pin, 5);
      
      // Apresenta dados no display
      set_display_while_charging();
      
      // Cronômetro
      add_timer();

      // Encerra o carregamento caso a tensão atinga o limiar superior
      if (battery_voltage > BAT_SUP_THRESHOLD)
        charged = true;

      delay(450);
    }

    // Sinaliza fim do período de carga
    play_buzzer(3, 750, 500);

    // Espera até que o botão up_button seja presisonado
    wait_for_up_button(false);
  }

// Classe Interface::apresenta dados no display enquanto carrega a bateria
  void Interface::set_display_while_charging(void)
  {
    // Limpa o display
    display.clearDisplay();
    
    // Aprenseta a TAG "CHARGING"
    display.setTextSize(1);
    display.setCursor(30, 0);
    display.print("C");
    display.setCursor(40, 0);
    display.print("H");
    display.setCursor(50, 0);
    display.print("A");
    display.setCursor(60, 0);
    display.print("R");
    display.setCursor(70, 0);
    display.print("G");
    display.setCursor(80, 0);
    display.print("I");
    display.setCursor(90, 0);
    display.print("N");
    display.setCursor(100, 0);
    display.print("G");

    // Apresenta contagem de tempo no display, ajustando o tamanho da string
    show_timer(25, 21);

    // Apresenta a tensão e corrente de teste da bateria
    display.setTextSize(1);
    display.setCursor(25, 56);
    display.print("["+String(battery_voltage)+"V]");
    display.print("["+String(discharge_current)+"A]");

    // Apresenta dados no display
    display.display();
  }

// Classe Interface::configura a corrente de descarga da bateria
  void Interface::set_discharge_current(void)
  {
    // Debounce
    set_display_set_current();
    
    // Espera até que o botão up_button seja pressionado por 1 segundo
    while (!up_button.pressedFor(1000))
    {
      // Apresenta dados no display
      set_output_voltage_on_display();
      
      // Verifica o estado dos botões
      up_button.read();
      down_button.read();

      // Aumenta a corrente de teste
      if (up_button.wasReleased() && voltage_control_output < voltage_max_output)
        voltage_control_output += 5;

      // Diminui a corrente de teste
      if (down_button.wasReleased() && voltage_control_output > voltage_min_output)
        voltage_control_output -= 5;
    }

    // Habilita saída de tensão
    analogWrite(voltage_control_pin, voltage_control_output);
  }

// Classe Interface::apresenta a tela de debounce para ajuste da tensão injetada no TBJ
  void Interface::set_display_set_current(void)
  {
    display.clearDisplay();
    display.setTextSize(1);
    display.setCursor(55, 15);
    display.print("SET");
    display.setCursor(53, 30);
    display.print("TEST");
    display.setCursor(45, 45);
    display.print("CURRENT");
    display.display();

    // DEBOUNCE
    delay(3000);
    up_button.read();
  }

// Classe Interface::apresenta a tensão injetada na base do TBJ no display
  void Interface::set_output_voltage_on_display(void)
  {
    // Converte a tensão de saída para a corrente de teste, em mA
    int test_current = (voltage_control_output * 21.1) - 2324;
    
    // Apresenta a corrente de teste
    display.clearDisplay();
    display.setTextSize(1);
    display.setCursor(1, 22);
    display.print("Discharge");
    display.setCursor(5, 36);
    display.print("Current: ");
    display.setTextSize(2);
    display.setCursor(70, 26);
    
    // Diferenciação entre medições mA e A
    if (test_current > 1999)
      display.print(String(test_current/1000)+","+String((test_current-2000)/100)+"A");
    else if (test_current > 999)
      display.print(String(test_current/1000)+","+String((test_current-1000)/100)+"A");
    else
      display.print("0,"+String(test_current/100)+"A");

    // Apresenta resultado
    display.display();
  }

// Classe Interface::inicializa a descarga da bateria
  void Interface::discharge_battery(void)
  {
    // Reseta as veriáveis do RTC
    reset_timer();

    // Realiza o teste de descarga
    while (!discharged)
    {
      // Lê a tensão da bateria
      battery_voltage = measure_voltage(battery_pin, 3);

      // Lê a tensão do coletor do TBJ
      v_drop_voltage = measure_voltage(v_drop_pin, 3);

      // Calcula corrente de descarga
      discharge_current = (battery_voltage - v_drop_voltage) / R_SHUNT;
      /*CRIAR FUNÇÃO PARA CORRIGIR A CORRENTE DE DESCARGA*/
      
      // Apresenta dados no display
      set_display_while_discharging();
      
      // Cronômetro
      add_timer();

      // Encerra o carregamento caso a tensão atinga o limiar inferior
      if (battery_voltage < BAT_INF_THRESHOLD)
        discharged = true;

      delay(286);
    }

    show_test_result();

   /* 
  while (Done == false)  {
     
 
  //*********************************************

    display.clearDisplay();
    display.setTextSize(2);
    display.setCursor(20,5);
    display.print(String(Hour) + ":" + String(Minute) + ":" + String(Second));

    display.setTextSize(1);
    display.setCursor(0,25);
    display.print("Disch Curr: ");   
    display.print(String(Current[voltage_control_output / 5])+"mA");    
    
    display.setCursor(2,40);   
    display.print("Bat Volt:" + String(BAT_Voltage)+"V" );

    Capacity =  (Hour * 3600) + (Minute * 60) + Second;
    Capacity = (Capacity * Current[voltage_control_output / 5]) / 3600;
    display.setCursor(2, 55);
    display.print("Capacity:" + String(Capacity) + "mAh");
    display.display();
    
    if (BAT_Voltage < Low_BAT_level)
    {
      Capacity =  (Hour * 3600) + (Minute * 60) + Second;
      Capacity = (Capacity * Current[voltage_control_output / 5]) / 3600;
      display.clearDisplay(); 
      display.setTextSize(2);      
      display.setCursor(2,15);     
      display.print("Capacity:");
      display.setCursor(2,40);  
      display.print(String(Capacity) + "mAh");
      display.display();
      Done = true;
      voltage_control_output = 0;
      analogWrite(voltage_control_pin, voltage_control_output);
      digitalWrite(buzzer_control_pin, HIGH);
      delay(100);
      digitalWrite(buzzer_control_pin, LOW);
      delay(100);
      digitalWrite(buzzer_control_pin, HIGH);
      delay(100);
      digitalWrite(buzzer_control_pin, LOW);
      delay(100);
    }
       delay(1000);
  }  */
  }

// Classe Interface::apresenta o tempo e capacidade, em tempo real, durante o teste
  void Interface::set_display_while_discharging(void)
  {
    // Limpa o display
    display.clearDisplay();
    
    // Aprenseta a TAG "hacking"
    display.setTextSize(1);
    display.setCursor(0, 0);
    display.print("H");
    display.setCursor(5, 10);
    display.print("A");
    display.setCursor(10, 19);
    display.print("C");
    display.setCursor(15, 28);
    display.print("K");
    display.setCursor(10, 37);
    display.print("I");
    display.setCursor(5, 46);
    display.print("N");
    display.setCursor(0, 56);
    display.print("G");

    // Apresenta contagem de tempo no display, ajustando o tamanho da string
    show_timer(25, 28);

    // Apresenta a tensão e corrente de teste da bateria
    display.setTextSize(1);
    display.setCursor(25, 56);
    display.print("["+String(battery_voltage)+"V]");
    display.print("["+String(discharge_current)+"A]");

    // Apresenta dados no display
    display.display();
  }

// Classe Interface::apresenta os resultados no display
  void Interface::show_test_result(void)
  { 
    // Cálcula a capacidade nominal, estimada
    unsigned int capacity;
    
    capacity = (hour * 3600) + (minute * 60) + second;
    capacity = (capacity * discharge_current) / 3600;

    // Apresenta a capacidade nominal, estimada
    display.clearDisplay();
    display.setTextSize(2);
    display.setCursor(5, 28);
    display.print("C");
    display.setTextSize(1);
    display.setCursor(16, 40);
    display.print("N");
    display.setTextSize(2);
    display.setCursor(22, 28);
    display.setTextSize(2);
    display.print(":");
    display.setTextSize(1);
    display.print(" ");
    display.setTextSize(2);
    display.print(String(capacity)+"mAh");
    display.display();

    // Espera até que o botão up_button seja presisonado
    wait_for_up_button(true);
  }

// Classe Interface::retorna o valor de tensão lido
  float Interface::measure_voltage(uint8_t pin, uint8_t t)
  {
    float voltage = 0.0;

    for (int i=0; i<100; i++)
    {
      voltage += (analogRead(pin)/100);
      delay(t);
    }
    
    return ((voltage * VCC / 1024.0) + 0.22);
  }

// Classe Interface::zera as veriáveis de controle do RTC
  void Interface::reset_timer(void)
  {
    second = 0;
    minute = 0;
    hour   = 0;
  }

// Classe Interface::incrementa as variáveis de controle do RTC
  void Interface::add_timer(void)
  {
    second++;

    if (second == 60)
    {
      second = 0;
      minute++;
    }
    
    if (minute == 60)
    {
      minute = 0;
      hour++;
    }
  }

// Classe Interface::apresenta a contagem de tempo 
  void Interface::show_timer(int x, int y)
  {
    // Apresenta o tempo gasto
    display.setTextSize(2);
    display.setCursor(y, x);
    
    if (hour < 10)
      display.print("0");
    display.print(String(hour)+":");

    if (minute < 10)
      display.print("0");
    display.print(String(minute)+":");

    if (second < 10)
      display.print("0");
    display.print(String(second));
  }

// Classe Interface::toca o buzzer N vezes (F = 1KHz)
  void Interface::play_buzzer(uint8_t N, uint16_t t_on, uint16_t t_off)
  {
    for (int i=0; i<N; i++)
    {
      analogWrite(buzzer_control_pin, 0xBF);
      delay(t_on);
      analogWrite(buzzer_control_pin, 0x00);
      delay(t_off);
    }
  }

// Classe Interface::espera que algum botão seja pressionado
  void Interface::wait_for_up_button(bool sound)
  {
    unsigned long t = millis() - 5000;
    
    while (!up_button.pressedFor(1000))
    {
      up_button.read();
      
      if (sound && (millis() > t + 5000))
      {
        play_buzzer(3, 500, 300);
        
        t = millis();
      }
    }
  }
