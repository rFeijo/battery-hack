// Bibliotecas
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <JC_Button.h>
#include <Adafruit_SSD1306.h>

#ifndef _INTERFACE_H_
  #define _INTERFACE_H_
  
  #define SCREEN_WIDTH  128        // Largura da tela do display OLED, em pixels
  #define SCREEN_HEIGHT 64         // Altura  da tela do display OLED, em pixels
  #define OLED_RESET    4          // Pino de reset do display OLED (-1 para utilizar o reset do Arduino)
  #define OLED_ADDR     0x3C       // Endereço I2C do display OLED

  #define vcc_pin       A0         // Pino conectado a fonte de alimentação (+5V)
  #define battery_pin   A1         // Pino conectado ao positivo da bateria
  #define v_drop_pin    A2         // Pino conectado ao coletor do TBJ

  #define R_SHUNT       1.2        // Resistência em serie com a bateria

  #define BAT_INF_THRESHOLD   3.15 // Tensão de mínima de descarga da bateria
  #define BAT_SUP_THRESHOLD   4.05 // Tensão de máxima de carga da bateria

  #define voltage_control_pin 10   // Pino conectado a base do TBJ
  #define voltage_max_output  205  // Máxima tensão de saída (4,02V -> 2A)
  #define voltage_min_output  115  // Máxima tensão de saída (2,25V -> 100mA)
  
  #define buzzer_control_pin  9    // Pino conectado ao Buzzer

  class Interface
  {
    public:
      void begin(void);
      void charge_battery(void);
      void discharge_battery(void);
      void set_discharge_current(void);

    private:
      void  add_timer(void);
      void  reset_timer(void);
      void  show_test_result(void);
      void  show_timer(int x, int y);
      void  set_display_set_current(void);
      void  wait_for_up_button(bool sound);
      void  set_display_while_charging(void);
      void  set_display_while_discharging(void);
      void  set_output_voltage_on_display(void);
      float measure_voltage(uint8_t pin, uint8_t t);
      void  play_buzzer(uint8_t N, uint16_t t_on, uint16_t t_off);

      uint8_t voltage_control_output = 115;
      byte    hour = 0, minute = 0, second = 0;
      bool    charged = false, discharged = false;
      float   VCC = 5.0, battery_voltage, v_drop_voltage, discharge_current;
  };
  
#endif /*_INTERFACE_H_*/
